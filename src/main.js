import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import axios from 'axios'


import {Utils} from './components/mixins/utils.js'

Vue.mixin(Utils);

Vue.config.productionTip = false;

window.axios = axios.create({
    baseURL: 'http://coop.api.netlor.fr/api',
    params: {
        token: false
    },
    headers: {
        Authorization: 'Token token=26cb1b2ca9fa4ab5b696245041806399'
    }
});

store.subscribe((mutation, state) => {
    localStorage.setItem('store', JSON.stringify(state));
});


new Vue({
    router,
    store,
    render: h => h(App),
    beforeCreate() {
        this.$store.commit('initialiseStore');
    },
}).$mount('#app');
