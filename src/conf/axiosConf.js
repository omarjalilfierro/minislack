import axios from 'axios'

window.axios = axios.create({
    baseURL: 'http://coop.api.netlor.fr/api',
    params : {
        token : false
    },
    headers: { Authorization: 'Token token=26cb1b2ca9fa4ab5b696245041806399' }
});