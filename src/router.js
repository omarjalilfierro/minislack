import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import CreateAccount from './views/SignUp.vue'
import Connexion from './views/Login.vue'
import Channels from "./views/Channels";
import Members from "./views/Members";
import Chat from "./views/Chat";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
          path: '/chat/:id',
          name: 'chat',
          component: Chat
        },
        {
            path: '/channels',
            name: 'channels',
            component: Channels
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '/createAccount',
            name: 'createAccount',
            component: CreateAccount
        },
        {
            path: '/connexion',
            name: 'connexion',
            component: Connexion
        },
        {
            path: '/members',
            name: 'members',
            component: Members
        }

    ]
})
