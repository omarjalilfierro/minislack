import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token : false,
        member : false,
        posts : false,
        channels : false,
    },
    mutations: {
        setToken(state,token) {
            state.token = token;
        },
        setMember(state,member) {
            state.member = member;
        },
        setPost(state, posts){
            state.posts = posts;
        },
        setChannels(state, channels){
            state.channels = channels;
        },
        initialiseStore(state) {
            if(localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        }
    },
    actions: {

    }
})
