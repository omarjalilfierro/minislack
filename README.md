# **Minislack**

- Minislack is a project where we had to make the frontend of an API 
 to which we did not have access.

- Main technologies I learned

1.  Axios
1.  Vue JS
1.  Bluma CSS
2.  Async data



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
